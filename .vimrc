" Comportamiento del editor
set nocompatible
syntax enable
filetype plugin indent on
set number
set nowrap
set linebreak
set showbreak=
set textwidth=100
set showmatch
set splitbelow
set splitright
set laststatus=2

" Opciones de búsqueda
set hlsearch
set smartcase
set incsearch

" Opciones de identación y formato
set autoindent
set shiftwidth=2
set smartindent
set smarttab
set softtabstop=2
set ruler
set backspace=indent,eol,start

" tecla leader
let mapleader = " "

" Mapeo de comandos
inoremap jj <ESC>l
map <leader>h :noh<CR>
map <leader>w :w<CR>
map <leader>W :w<CR>
map <leader>q :q<CR>
map <leader>Q :q<CR>
inoremap (<CR> ()<ESC>i<CR><CR><Up><C-f>
inoremap {<CR> {}<ESC>i<CR><CR><Up><C-f>
inoremap [<CR> []<ESC>i<CR><CR><Up><C-f>
"" Borrar el buffer actual
map <leader>c :bp<bar>sp<bar>bn<bar>bd<CR>
"" Borrar todos los buffers menos el actual
map <leader>C :%bd<bar>e #<CR>
map <leader>r :set nu rnu!<CR>
map <leader>ff :find 
map <leader>fb :b 
map <leader>fn :e 
map L :tabnext<CR>
map H :tabnext<CR>
nnoremap <C-j> <C-W><C-J>
nnoremap <C-k> <C-W><C-K>
nnoremap <C-l> <C-W><C-L>
nnoremap <C-h> <C-W><C-H>
map <C-Up> :resize +3 <CR>
map <C-Down> :resize -3 <CR>
map <C-Right> :vertical resize +3 <CR>
map <C-Left> :vertical resize -3 <CR>

" Habilitar búsqueda de archivos recursiva
set path+=**
set wildmenu

" Configuración de netrw
let g:netrw_banner=0	    " Desactivar banner
let g:netrw_liststyle=3	    " Vista de árbol

" Cambiar cursor en modo de inserción
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" Resaltar línea actual
hi CursorLineNR cterm=bold
highlight CursorLine cterm=NONE ctermbg=NONE ctermfg=NONE guibg=NONE guifg=NONE
set cursorline
