# Trabajo de mongo sin IDE
Este repositorio es material complementario de
[mi presentación sobre interactuar con MongoDB en java](https://docs.google.com/presentation/d/1SFRK0fvNfJKM5v_AxAb3dmK41cJ--5-yO5NM2svIw70/edit?usp=sharing).

## Contenido del repositorio
- Configuración de vim en `.vimrc`. Se ha de copiar en tu directorio personal.
- Proyecto de maven `mongo/`. Crea, actualiza y borra un nuevo producto, mostrándolos todos cada vez.

## Dependencias
- MongoDB Community Edition. Manual de instalación [aquí](https://www.mongodb.com/docs/v5.0/installation/).
- `maven`.
- JDK (Recomendado JDK >= 11).

## Compilar y ejecutar
Para compilar el proyecto, dirígete al directorio raíz del proyecto y ejecuta `mvn clean compile assembly:single`.
Para ejecutarlo ejecuta `java -jar target/mongo.jar` desde el directorio raíz del proyecto.
