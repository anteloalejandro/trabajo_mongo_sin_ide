package local.alejandro;
import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import com.mongodb.client.result.*;

import org.bson.Document;
import org.bson.types.ObjectId;
import java.util.*; // Mongo requiere List, Arrays y ArrayList

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

public class App {
  public static void main( String[] args ) {
    // Crear conexión con la base de datos
    MongoClient client = MongoClients.create("mongodb://localhost:27017");
    MongoDatabase db = client.getDatabase("tienda");
    MongoCollection<Document> productos = db.getCollection("productosJava");
    
    // Insertar dato
    Document producto = new Document();
    producto.put("referencia", "P0004");
    producto.put("tipo", "Falda");
    producto.put("paraMujer", true);
    producto.put("talla", "L");
    producto.put("precio", 10.50);
    producto.put("otras características", new Document("color", "naranja").append("patron", "a cuadros"));
    productos.insertOne(producto);
    // La siguiente línea hace lo mismo pero guarda la id del objeto por si se quisiese referenciar después
    // ObjectId id = productos.insertOne(producto).getInsertedId().asObjectId().getValue();
    System.out.println("\n--DATO INSERTADO--");
    findAll(productos);

    // Actualizar datos
    Document query = new Document();
    query.put("precio", new Document("$lt", 25));
    Document update = new Document();
    update.put("$mul", new Document("precio", 1.1));
    update.put("$set", new Document("talla", "S"));
    productos.updateMany(query, update);
    System.out.println("\n--DATO ACTUALIZADO--");
    findAll(productos);

    // Borrar datos
    query = new Document();
    query.put("referencia", "P0004");
    productos.deleteMany(query);
    System.out.println("\n--DATO BORRADO--");
    findAll(productos);

    // Mostrar todos los articulos para mujer
    System.out.println("\n--ARTÍCULOS PARA MUJER--");
    query = new Document();
    query.put(
      "$or", Arrays.asList(
	new Document("paraHombre", false),
	new Document("paraMujer", true)
      )
    );
    MongoCursor<Document> cursor = productos.find(query).iterator();
    while (cursor.hasNext()) {
      System.out.println(cursor.next().toJson());
    }
    System.out.println("------------------------\n");

  }

  //Mostrar todos los campos
  public static void findAll(MongoCollection<Document> col) {
    MongoCursor<Document> cursor = col.find().iterator();
    while (cursor.hasNext()) {
      System.out.println(cursor.next().toJson());
    }
  }
}
